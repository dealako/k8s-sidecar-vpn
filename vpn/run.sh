#!/usr/bin/env bash

for file in colors.sh logger.sh utils.sh config.sh; do
    [[ -f ${file} ]] && source ${file} || echo "Unable to load ${file}"
done

log "Running image with tag ${_Y}${tag}${_W}"
docker run -d --name sidecar-vpn --privileged ${tag}

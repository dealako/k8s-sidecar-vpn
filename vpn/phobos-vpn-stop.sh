#!/usr/bin/env bash

USER_HOME="/vpn"
APP_HOME="${USER_HOME}/app"

for file in colors.sh logger.sh utils.sh; do
    [[ -f ${USER_HOME}/${file} ]] && source ${USER_HOME}/${file} || echo "Unable to load ${USER_HOME}/${file}"
done

#--------------------------------------------------------------------------
# Stop Rackspace Phobos Network VPN
#--------------------------------------------------------------------------
function phobos_vpn_stop() {
  declare -r log_file="${USER_HOME}/.openconnect.log"
  declare -r pid_file="${USER_HOME}/.openconnect.pid"

  if [[ -f "${pid_file}" ]]; then
    log "Stopping Phobos VPN..."
    sudo kill -2 $(cat "${pid_file}") && rm -f "${pid_file}"
  else
    log_warn "Unable to stop Phobos VPN - ${_Y}${pid_file}${_W} pid file does not exist. Is it running??"
  fi
}

phobos_vpn_stop $@

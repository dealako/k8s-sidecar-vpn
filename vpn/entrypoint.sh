#!/bin/bash

export USER_HOME=/vpn
export APP_HOME=${USER_HOME}/app

for file in colors.sh logger.sh utils.sh; do
    [[ -f ${USER_HOME}/${file} ]] && source ${USER_HOME}/${file} || echo "Unable to load ${USER_HOME}/${file}"
done

log "Running entrypoint.sh..."

vpn_start_script="${APP_HOME}/phobos-vpn-start.sh"
[[ -f ${vpn_start_script} ]] || die "Unable to locate VPN start script: ${_Y}${vpn_start_script}${_W}"

log "Connecting to the phobos network..."
${vpn_start_script} $@

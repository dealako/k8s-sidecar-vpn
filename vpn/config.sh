#!/usr/bin/env bash

# Common configuration settings

declare -r org="rcbops"
declare -r repo="k8s-sidecar-vpn"
declare -r version="test"
declare -r tag="${org}/${repo}:${version}"

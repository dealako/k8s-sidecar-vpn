#!/usr/bin/env bash

USER_HOME="/vpn"
APP_HOME="${USER_HOME}/app"

for file in colors.sh logger.sh utils.sh; do
    [[ -f ${USER_HOME}/${file} ]] && source ${USER_HOME}/${file} || echo "Unable to load ${USER_HOME}/${file}"
done

#--------------------------------------------------------------------------
# Start Rackspace Phobos Network VPN
# params:
#  - gateway host or IP
#--------------------------------------------------------------------------
function phobos_vpn_start() {
  if [[ $# -eq 0 ]]; then
    log_warn "Missing gateway input argument to ${0}. Please provide and re-run start routine"
    return 1
  fi

  if [[ "${1}x" == "x" ]]; then
    log_warn "Gateway value is empty. Please set a value and re-run start routine."
    return 1
  fi

  declare -r gateway="$1"
  declare -r config_file="${USER_HOME}/.openconnect/.openconnect_config"
  declare -r pw_file="${USER_HOME}/.openconnect/.openconnect_pw"
  declare -r log_file="${USER_HOME}/.openconnect.log"
  declare -r pid_file="${USER_HOME}/.openconnect.pid"

  [[ -f "${config_file}" ]] || die "Unable to read openconnect configuration file: ${_Y}${config_file}${_Y}. Please create and re-run start routine."
  [[ -f "${pw_file}" ]] || die "Unable to read phobos VPN authentication file: ${_Y}${pw_file}${_Y}. Please create and re-run start routine."

  touch ${log_file}
  log "Starting Phobos VPN to gateway host ${gateway}..." | tee -a ${log_file}
  log "Using config ${config_file}..." | tee -a ${log_file}
  log "Logging to: ${log_file}..." | tee -a ${log_file}
  log "PID will be stored in: ${pid_file}" | tee -a ${log_file}

  # Additional options:
  # --background
  # --juniper
  # --verbose

  #cat ${pw_file} | sudo openconnect \
  #  --background \
  #  --pid-file=${pid_file} \
  #  --timestamp \
  #  --user=${user} \
  #  --no-dtls \
  #  --authgroup=${auth_group} \
  #  --servercert sha256:7b08175d3cc45be419e6ff35844c9265776631ecd1640501c92d37991f70623d \
  #  ${gateway} \
  #  --passwd-on-stdin | tee -a ${log_file}

  cat ${pw_file} | sudo openconnect \
    --config="${config_file}" \
    --pid-file=${pid_file} \
    ${gateway} | tee -a ${log_file}
}

# Invoke VPN start with the gateway host or ip
phobos_vpn_start $@

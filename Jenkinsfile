def dockerHost = '/var/run/docker.sock'
def dockerRegistry = 'docker.io'
def dockerRegistryURL = 'https://index.docker.io/v1/'
def dockerRegistryOrg = 'rcbops'

pipeline {
    agent {
        node {
            label "nodepool-ubuntu-bionic-g1-8"
        }
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '10', daysToKeepStr: '7'))
        disableConcurrentBuilds()
    }

    environment {
        CI_REGISTRY_IMAGE_APP = "${dockerRegistry}/${dockerRegistryOrg}/k8s-sidecar-app"
        CI_REGISTRY_IMAGE_VPN = "${dockerRegistry}/${dockerRegistryOrg}/k8s-sidecar-vpn"
    }

    parameters {
        // string(name: "PIPELINE_BRANCH", defaultValue: '', description: 'branch to run the pipeline on, eg. develop')
        // string(name: "TAG", defaultValue: '', description: 'optionally tag generated images and push to registry')
        // booleanParam(name: 'TAG_LATEST', defaultValue: false, description: 'tag image as \'latest\'')
        booleanParam(name: 'KEEP_CONTAINERS', defaultValue: false, description: 'Keep containers running after job has finished?')
    }

    stages {
        stage('Setup') {
            steps {
                echo 'Setting up the host environment...'
                sh 'sudo apt-get update && sudo apt install docker.io -y'
                sh 'systemctl start docker'
                sh 'systemctl enable docker'
                sh 'docker --version'
            }
        }

        stage('Checkout') {
            steps {
                dir('re-platform') {
                    checkout scm

                    script {
                        env.CI_COMMIT_SHA=sh(returnStdout: true, script: "git rev-parse --verify HEAD").trim()
                    }
                }
            }
        }

        stage('Build and Publish Image - SHA') {
            steps {
                withCredentials([usernamePassword(
                                credentialsId: 'dockerhubrpcjirasvc',
                                passwordVariable: 'dockerHubPassword',
                                usernameVariable: 'dockerHubUser')]) {
                    dir('re-platform/app') {
                        echo "Logging into the docker registry with user ${env.dockerHubUser} to registry ${dockerRegistryURL}..."
                        sh "docker login -u ${env.dockerHubUser} -p ${env.dockerHubPassword} ${dockerRegistryURL}"

                        echo "Building ${env.CI_REGISTRY_IMAGE_APP}:${env.CI_COMMIT_SHA}..."
                        sh "docker build -t ${env.CI_REGISTRY_IMAGE_APP}:${env.CI_COMMIT_SHA} ."

                        echo "Publishing '${env.CI_REGISTRY_IMAGE_APP}:${env.CI_COMMIT_SHA}'..."
                        sh "docker push ${env.CI_REGISTRY_IMAGE_APP}:${env.CI_COMMIT_SHA}"
                    }

                    dir('re-platform/vpn') {
                        echo "Logging into the docker registry with user ${env.dockerHubUser} to registry ${dockerRegistryURL}..."
                        sh "docker login -u ${env.dockerHubUser} -p ${env.dockerHubPassword} ${dockerRegistryURL}"

                        echo "Building ${env.CI_REGISTRY_IMAGE_VPN}:${env.CI_COMMIT_SHA}..."
                        sh "docker build -t ${env.CI_REGISTRY_IMAGE_VPN}:${env.CI_COMMIT_SHA} ."

                        echo "Publishing '${env.CI_REGISTRY_IMAGE_VPN}:${env.CI_COMMIT_SHA}'..."
                        sh "docker push ${env.CI_REGISTRY_IMAGE_VPN}:${env.CI_COMMIT_SHA}"
                    }
                }
            }
        }

        stage('Build and Publish Image - Latest') {
            when {
                branch 'master'
            }

            steps {
                withCredentials([usernamePassword(
                                credentialsId: 'dockerhubrpcjirasvc',
                                passwordVariable: 'dockerHubPassword',
                                usernameVariable: 'dockerHubUser')]) {
                    dir('re-platform/app') {
                        echo "Logging into the docker registry with user ${env.dockerHubUser} to registry ${dockerRegistryURL}..."
                        sh "docker login -u ${env.dockerHubUser} -p ${env.dockerHubPassword} ${dockerRegistryURL}"

                        echo "Building ${env.CI_REGISTRY_IMAGE_APP}:${env.CI_COMMIT_SHA}..."
                        sh "docker build -t ${env.CI_REGISTRY_IMAGE_APP}:${env.CI_COMMIT_SHA} ."

                        sh "docker tag ${env.CI_REGISTRY_IMAGE_APP}:${env.CI_COMMIT_SHA} ${env.CI_REGISTRY_IMAGE_APP}:latest"
                        echo "Publishing ${env.CI_REGISTRY_IMAGE_APP}:latest..."
                        sh "docker push ${env.CI_REGISTRY_IMAGE_APP}:latest"
                    }

                    dir('re-platform/vpn') {
                        echo "Logging into the docker registry with user ${env.dockerHubUser} to registry ${dockerRegistry}..."
                        sh "docker login -u ${env.dockerHubUser} -p ${env.dockerHubPassword} ${dockerRegistry}"

                        echo "Building ${env.CI_REGISTRY_IMAGE_VPN}:${env.CI_COMMIT_SHA}..."
                        sh "docker build -t ${env.CI_REGISTRY_IMAGE_VPN}:${env.CI_COMMIT_SHA} ."

                        sh "docker tag ${env.CI_REGISTRY_IMAGE_VPN}:${env.CI_COMMIT_SHA} ${env.CI_REGISTRY_IMAGE_VPN}:latest"
                        echo "Publishing ${env.CI_REGISTRY_IMAGE_VPN}:latest..."
                        sh "docker push ${env.CI_REGISTRY_IMAGE_VPN}:latest"
                    }
                }
            }
        }
    }
}

#!/usr/bin/env bash

# This is a convenience script to build the docker image.

for file in colors.sh logger.sh utils.sh config.sh; do
    [[ -f ${file} ]] && source ${file} || echo "Unable to load ${file}"
done

log "Building image with tag ${_Y}${tag}${_W}"
docker build --tag ${tag} .

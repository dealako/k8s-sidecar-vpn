# Kubernetes Sidecar VPN

This project creates a prototype/test kubernetes deployment to validate a VPN
side-car deployment scenario. This design pattern is used to run an
application which requires a private VPN connection to a remote network.
Normally, both the application and the VPN connection logic would co-exist on
the same virtual machine. In the docker/container world, the focus is to
separate out these two constructs so that they can be managed, versioned,
patched/updated, and maintained separately.
 
Multi-container pods have a few advantages including:
 
* private communication between containers in the pod
* shared volumes (writing to a shared file or directory)
* shared networking
 
Common uses for additional containers, sometimes generically referred to as a 'sidecar' containers, are:
 
* logging utilities, sync services, watchers, and monitoring agents. For example, a benefit of moving the logging work
  to a separate container is that if the logging code is faulty, the fault will be isolated to that container — an
  exception thrown in the nonessential logging code won't bring down the main application.
* a separate container can be used via an adapter pattern to standardize application output or monitoring 
  data for aggregation.
* an additional container can be used to proxy specific network traffic through a VPN. In this case, the VPN client and
  related configuration would reside in a separate container - the details would be transparent to the main container.
   
The main deployment consists of two docker containers:

1. An app container which simply queries OpenStack in a loop to obtain a list
   of existing images
1. A sidecar container that connects to a remote network via a VPN connection

The app container logic will succeed if the VPN connection is established.

In this case, we'll lever the Ambassador Design Pattern as depicted in this
borrowed diagram.

![Multi-Container Design](images/multi-container-pod-design.png)

> Source: [Multi-Container Pod Design Patterns in Kubernetes](https://matthewpalmer.net/kubernetes-app-developer/articles/multi-container-pod-design-patterns.html)

Specifically, the VPN sidecar container will manage the network connection
to the remote VPN server.

## Project Structure

The project structure consists of three primary folders:

* **app** - a docker mini-project for the simple OpenStack client. Contains docker
  build, tag and publish helper scripts.
* **vpn** - a docker mini-project for running the `openconnect` VPN client
* **deployments** - a helm chart for deploying resources into kubernetes

## Assumptions

* You have previously connected to a running kubernetes cluster and have
  `kubectl` access. This may be a local `minikube` deployment or a remote
  cluster of your choosing.
* You have `helm` installed and configured on your local machine.

## Build and Publish the App Docker Image

```bash
cd app
# Optionally, review the config.sh to change the docker tag details
./build.sh
./publish.sh
```

## Build the VPN Docker Image

```bash
cd vpn
# Optionally, review the config.sh to change the docker tag details
./build.sh
./publish.sh
```

## Deployment

To deploy the helm chart, [see the deployment documentation](deployments/charts/k8s-sidecar-vpn/README.md) in the deployments folder.

## CI/CD

`Jenkinsfile` and `.gitlab-ci.yaml` files were created to build and publish docker images automatically.

## References

* [Multi-Container Pod Design Patterns in Kubernetes](https://matthewpalmer.net/kubernetes-app-developer/articles/multi-container-pod-design-patterns.html)
